export default {
  cake: {
    56: '0x94dF36a61B489c95E66A830c366528FF063687b4',
    97: '',
    137:"0x287bcdda926400AE1FA65d3E90Cb2fc47bc39cB3",
  },
  masterChef: {
    56: '0x26E55c521E86851C21fa56b00DB764c434FaB7B6',
    97: '',
    137:"0x2F3073707a4C13396Ed31eF94938A28940e58dFB"
  },
  wbnb: {
    56: '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c',
    97: '',
    137:"0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270"
  },
  lottery: {
    56: '',
    97: '',
    137: "",
  },
  lotteryNFT: {
    56: '',
    97: '',
    137: "",
  },
  mulltiCall: {
    56: '0x1ee38d535d541c55c9dae27b12edf090c608e6fb',
    97: '0x67ADCB4dF3931b0C5Da724058ADC2174a9844412',
    137:"0x11ce4B23bD875D7F5C6a31084f55fDe1e9A87507",
  },
  busd: {
    56: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
    97: '',
    137:"0xc2132D05D31c914a87C6611C10748AEb04B58e8F",
  },
}
