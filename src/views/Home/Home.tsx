/* tslint:disable-next-line */
import React from 'react'
import styled from 'styled-components'
import { Heading, Text, BaseLayout, CardRibbon, CardBody, Card } from '@pancakeswap-libs/uikit'
import useI18n from 'hooks/useI18n'
import Page from 'components/layout/Page'
import Container from 'components/layout/Container'
import FarmStakingCard from './components/FarmStakingCard'
import LotteryCard from './components/LotteryCard'
import CakeStats from './components/CakeStats'
import TotalValueLockedCard from './components/TotalValueLockedCard'
import TwitterCard from './components/TwitterCard'
import MetaMask from './components/Metamask'

const Hero = styled.div`
  align-items: center;
  background-image: url('/images/elmer/3.png');
  background-repeat: no-repeat;
  background-position: top center;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  padding-top: 116px;
  text-align: center;

  ${({ theme }) => theme.mediaQueries.lg} {
    background-image: url("${process.env.PUBLIC_URL}/images/elmer/logoonlytext.png");
    background-position: center;
    height: 130px;
    padding-top: 0;
  }
`

/*
const Hero = styled.div`
  align-items: center;
  background-image: url('/images/elmer/3.png');
  background-repeat: no-repeat;
  background-position: top center;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  padding-top: 116px;
  text-align: center;

  ${({ theme }) => theme.mediaQueries.lg} {
    background-image: url('/images/elmer/3.png'), url('/images/elmer/3b.png');
    background-position: left center, right center;
    height: 165px;
    padding-top: 0;
  }
`
*/

const Cards = styled(BaseLayout)`
  align-items: stretch;
  justify-content: stretch;
  margin-bottom: 48px;

  & > div {
    grid-column: span 6;
    width: 100%;
  }

  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    & > div {
      grid-column: span 6;
    }
  }
`


  /*
  
  <Heading as="h1" size="xl" mb="24px" color="secondary">
    Foray
  </Heading>
  <Text>The best game on Polygon!</Text>

  */
const PageHome = styled(Container)`
  
  padding-top: 16px;
  padding-bottom: 1px;

`

const Home: React.FC = () => {
  const TranslateString = useI18n()

  return (
    <div> 
      <PageHome>
        <Hero/>
        <div>
          <Cards>
            {/* <FarmStakingCard />
            <TwitterCard/>
            <MetaMask/> */} 
            <TotalValueLockedCard />
            <CakeStats />
          </Cards>
        </div>

        <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'space-between',
      paddingBottom: "26px" ,}}> 
        
        <Card ribbon={<CardRibbon variantColor="failure" text="PlantNFT" />}>
          <img style={{ height: "150px" }} src={`${process.env.PUBLIC_URL}/images/elmer/planetNFT.png`} alt="My logo" />
        </Card>

        <Card ribbon={<CardRibbon variantColor="failure" text="FlagNFT" />}>
          <img style={{ height: "150px" }} src={`${process.env.PUBLIC_URL}/images/elmer/flagNFT.png`} alt="My logo" />
        </Card>
        <Card ribbon={<CardRibbon variantColor="failure" text="SoldierNFT" />}>
          <img style={{ height: "150px" }} src={`${process.env.PUBLIC_URL}/images/elmer/soldierNFT.jpg`} alt="My logo" />
        </Card>
        <Card ribbon={<CardRibbon variantColor="failure" text="MartianLotto" />}>
          <img style={{ height: "150px" }} src={`${process.env.PUBLIC_URL}/images/elmer/martianLotto.jpg`} alt="My logo" />
        </Card>
      </div>

      </PageHome>
      
      
      <div style={{width: '100vw', paddingTop: 56, paddingBottom: 32, backgroundColor: 'rgba(27.59, 32.70, 40.37, 1)', display: 'inline-flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start',}}>
    <div style={{width: '50vw', paddingLeft: 40, paddingRight: 40, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
        <div style={{width: '100vw', display: 'inline-flex', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
            <div style={{flex: '1 1 0%', display: 'inline-flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                <p style={{opacity: 0.50, width: 140, fontSize: 12,  letterSpacing: 0.36, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)', textTransform: 'uppercase',}}>About</p>
                <div style={{height: 8,}}/>
                <div style={{width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Contact</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Blog</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Community</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Token</p>
                    <div style={{height: 4,}}/>
                    <p style={{width: '100%', fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>—</p>
                    <div style={{height: 4,}}/>
                    <div style={{width: '100%', display: 'inline-flex', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                        <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(248.63, 185.72, 111.88, 1)',}}>Online Store</p>
                    </div>
                </div>
            </div>
            <div style={{width: 24,}}/>
            <div style={{flex: '1 1 0%', display: 'inline-flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                <p style={{opacity: 0.50, width: 140, fontSize: 12,  letterSpacing: 0.36, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)', textTransform: 'uppercase',}}>Help</p>
                <div style={{height: 8,}}/>
                <div style={{width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                    <p style={{width: '100%', fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Customer Support</p>
                    <div style={{height: 4,}}/>
                    <p style={{width: '100%', fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Troubleshooting</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Forum</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Guides</p>
                </div>
            </div>
            <div style={{width: 24,}}/>
            <div style={{flex: '1 1 0%', display: 'inline-flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                <p style={{opacity: 0.50, width: 140, fontSize: 12,  letterSpacing: 0.36, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)', textTransform: 'uppercase',}}>Developers</p>
                <div style={{height: 8,}}/>
                <div style={{width: '100%', display: 'flex', flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Github</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Documentation</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Bug Bounty</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Audits</p>
                    <div style={{height: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Careers</p>
                </div>
            </div>
            <div style={{width: 24,}}/>
            <div style={{flex: '1 1 0%', height: '100%', position: 'relative',}}>
                <div style={{width: 194, height: 30, left: 68, top: 0, position: 'absolute', display: 'inline-flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}/>
                <div style={{width: 188.46, height: 31.96, left: 74, top: 0, position: 'absolute', display: 'inline-flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end',}}>
                    <img alt="Profile" style={{width: 34, height: 31.96, borderRadius: 8,}} src="https://via.placeholder.com/34x31.960067749023438"/>
                    <div style={{width: 23,}}/>
                    <img alt="Profile" style={{width: 131.46, height: 16.62, borderRadius: 8,}} src="https://via.placeholder.com/131.45521545410156x16.62290382385254"/>
                </div>
            </div>
        </div>
        <div style={{height: 32,}}/>
        <div style={{width: 262, flex: '1 1 0%', display: 'inline-flex', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start',}}>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
            <div style={{width: 16,}}/>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
            <div style={{width: 16,}}/>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
            <div style={{width: 16,}}/>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
            <div style={{width: 16,}}/>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
            <div style={{width: 16,}}/>
            <img alt="Profile"style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
        </div>
        <div style={{height: 32,}}/>
        <div style={{width: '100%', display: 'inline-flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}}>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',}}>
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',}}>
                    <img alt="Profile" style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
                    <div style={{width: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>EN/USD</p>
                </div>
                <div style={{width: 16,}}/>
                <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',}}>
                    <img alt="Profile" style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
                    <div style={{width: 4,}}/>
                    <p style={{fontSize: 16, lineHeight: '120%', color: 'rgba(193.37, 193.37, 193.37, 1)',}}>Settings</p>
                </div>
            </div>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',}}>
                <div style={{width: 30, height: 30, backgroundColor: 'white', borderRadius: 8,}}/>
                <div style={{width: 16,}}/>
                <p style={{fontSize: 16,  letterSpacing: 0.48, lineHeight: '120%', textAlign: 'center', color: 'white',}}>NEBULA Price: $1234.88</p>
                <div style={{width: 16,}}/>
                <div style={{paddingLeft: 12, paddingRight: 12, paddingTop: 4, paddingBottom: 4, backgroundColor: 'rgba(248.63, 185.72, 111.88, 1)', boxShadow: '0px -2px 0px rgba(0, 0, 0, 0.10) inset', borderRadius: 16, display: 'inline-flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center',}}>
                    <div style={{height: 24, display: 'inline-flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',}}>
                        <p style={{fontSize: 16,  letterSpacing: 0.48, lineHeight: '120%', textAlign: 'center', color: 'white',}}>Buy NEBULA</p>
                        <div style={{width: 4,}}/>
                        <img alt="Profile" style={{width: 24, height: 24, borderRadius: 8,}} src="https://via.placeholder.com/24x24"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      {/* <iframe   allowFullScreen title="My Daily Marathon Tracker" style={{border: "0px" }} width="100%" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2F8WSXSKq41dTUwx7qRQvX1o%2FUntitled%3Fnode-id%3D1%253A104%26scaling%3Dmin-zoom%26page-id%3D0%253A1%26hide-ui=1" />
      */}
    </div>  
  )
}

export default Home
