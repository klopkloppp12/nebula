import React, { useEffect , useState,} from 'react'
import styled from 'styled-components'
import { Card, CardBody, BaseLayout,  Heading, Text,Input  } from '@pancakeswap-libs/uikit'
import Page from 'components/layout/Page'

import FarmStakingCard from '../Home/components/FarmStakingCard'
import LotteryCard from '../Home/components/LotteryCard'
import CakeStats from '../Home/components/CakeStats'
import TotalValueLockedCard from '../Home/components/TotalValueLockedCard'
import TwitterCard from '../Home/components/TwitterCard'
import MetaMask from '../Home/components/Metamask'
import BuyNFT from '../Home/components/BuyNFT'

const Hero = styled.div`
  align-items: center;
  background-image: url('/images/elmer/3.png');
  background-repeat: no-repeat;
  background-position: top center;
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: auto;
  margin-bottom: 32px;
  padding-top: 116px;
  text-align: center;

  ${({ theme }) => theme.mediaQueries.lg} {
    background-image: url("${process.env.PUBLIC_URL}/images/elmer/logotextbottom.png");
    background-position: center;
    height: 165px;
    padding-top: 0;
  }
`
const Cards = styled(BaseLayout)`
  align-items: stretch;
  justify-content: stretch;
  margin-bottom: 48px;

  & > div {
    grid-column: span 6;
    width: 100%;
  }

  ${({ theme }) => theme.mediaQueries.sm} {
    & > div {
      grid-column: span 8;
    }
  }

  ${({ theme }) => theme.mediaQueries.lg} {
    & > div {
      grid-column: span 6;
    }
  }
`

const Profile: React.FC = () => {
	const [value, setValue] = useState("");

	const handleSubmit = () => {
    alert(value);
  }
	return (
		<Page>
		 {/* <Hero/> */}
        <div>
          <Cards>
            <FarmStakingCard />
            <TwitterCard/>
            <MetaMask/>
						<BuyNFT/>
          </Cards>
        </div>

		</Page>
	)

}


export default Profile