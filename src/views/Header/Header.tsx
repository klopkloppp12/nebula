import React, { useEffect , useState,} from 'react'
import { Link, useLocation, useHistory } from 'react-router-dom'
import { useWallet } from '@binance-chain/bsc-use-wallet'
import { Button, ButtonMenuItem, ButtonMenu,useWalletModal, Dropdown, IconButton, LogoIcon,Flex} from '@pancakeswap-libs/uikit'

const Header : React.FC<unknown> = () => {
	const { account, connect, reset } = useWallet()
	const history = useHistory();

	useEffect(() => {
		if (!account && window.localStorage.getItem('accountStatus')) {
		  connect('injected')
		}
	}, [account, connect])

	const {  onPresentAccountModal } = useWalletModal(
		() => null,
		() => null,

		account
	);

	const { onPresentConnectModal } = useWalletModal(connect, reset);

	const [activeIndex, setActiveIndex] = useState(0);

	const location = useLocation();
	const loc = location.pathname;
	useEffect(() => {
		console.log(loc);
		switch(loc) {
			case '/farms':
				setActiveIndex(1);
				break;
			case '/pools':
				setActiveIndex(2);
				
				break;
			case '/lootbox':
				setActiveIndex(3);
				break;
			case '/':
				setActiveIndex(0);
				break;
			default:
				setActiveIndex(-1);
				console.log(loc);
				break;
		} 
	}, [loc]);
	
	const link = (name) => {history.push(name)};

    return (

    <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'space-between'}}> 

			<div style={{alignSelf: 'center'}}>
				<img src={`${process.env.PUBLIC_URL}/images/elmer/logoonly.png`} alt="My logo" />
			</div>

			<ButtonMenu activeIndex={activeIndex} >
					<ButtonMenuItem as="a" onClickCapture={() => { link("/")}}>
						Home
					</ButtonMenuItem>
					<ButtonMenuItem as="a" onClickCapture={() => { /* link("/farms") */}}>
						Farms
					</ButtonMenuItem>
					<ButtonMenuItem as="a"onClickCapture={() => { /* link("/pools") */}}>
						Pool
					</ButtonMenuItem>
					
			</ButtonMenu>

			<div> 
				<Flex style={{paddingRight:"10px"}}> 
					
					<IconButton onClickCapture={() => { link("/profile")}}>
						<img  style={{width:"80%"}} src={`${process.env.PUBLIC_URL}/images/elmer/1077063.png`}   alt="Profile"/>
					</IconButton> 
					

					<div style={{paddingLeft:"10px"}}>
						{
						account? 
						<Button onClick={onPresentAccountModal}>Open Wallet</Button>:
						<Button onClick={onPresentConnectModal}>Connect</Button> }
					</div>
				
				</Flex>
			</div>
			
	
		</div>

      
    )
  }
  // add the address on openaccount 
  export default Header