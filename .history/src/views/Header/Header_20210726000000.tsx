import React, { useEffect , useState} from 'react'
import { Button, ButtonMenuItem, ButtonMenu, Heading, Text, LogoIcon } from '@pancakeswap-libs/uikit'




const Header : React.FC<unknown> = () => {
		const [activeIndex, setActiveIndex] = useState(0);
		
		
    return (
    <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center'}}> 

			<ButtonMenu activeIndex={activeIndex} >
				<ButtonMenuItem as="a" href="/" onClick={() => setActiveIndex(0)} >
					Home
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/farms"onClick={() => setActiveIndex(1)} >
					Farms
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/pools"onClick={() => setActiveIndex(2)} >
					Pool
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/"onClick={() => setActiveIndex(0)} >
					Lootboxes
				</ButtonMenuItem>
			</ButtonMenu>
		</div>

      
    )
  }
  
  export default Header