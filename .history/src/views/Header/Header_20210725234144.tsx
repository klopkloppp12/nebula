import React from 'react'
import styled from 'styled-components'
import { Button, ButtonMenuItem, ButtonMenu, Heading, Text, LogoIcon } from '@pancakeswap-libs/uikit'
import Page from 'components/layout/Page'
import useI18n from 'hooks/useI18n'



const Header : React.FC<unknown> = () => {
  
    return (
    <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center'}}> 

			<ButtonMenu activeIndex={0} >
				<ButtonMenuItem as="a" href="/">
					Home
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/farms">
					Farms
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/pools">
					Pool
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/">
					Lootboxes
				</ButtonMenuItem>
			</ButtonMenu>
		</div>

      
    )
  }
  
  export default Header