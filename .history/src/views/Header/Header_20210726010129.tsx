import React, { useEffect , useState} from 'react'
import { Button, ButtonMenuItem, ButtonMenu, Heading, Text, LogoIcon } from '@pancakeswap-libs/uikit'


import { useLocation } from 'react-router-dom'

const Header : React.FC<unknown> = () => {
		const [activeIndex, setActiveIndex] = useState(0);
		const location = useLocation();
		const loc = location.pathname
  	console.log(loc);
		
		
		switch(loc) {
			case '/farms':
				setActiveIndex(1);
				break;
			case '/pool':
				setActiveIndex(2);
				break;
			case '/lootbox':
				setActiveIndex(3);
				break;
			default:
				setActiveIndex(0);
				break;
		} 
    return (
    <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center'}}> 

			<ButtonMenu activeIndex={0} >
				<ButtonMenuItem as="a" href="/" >
					Home
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/farms" >
					Farms
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/pools" >
					Pool
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/" >
					Lootboxes
				</ButtonMenuItem>
			</ButtonMenu>
		</div>

      
    )
  }
  
  export default Header