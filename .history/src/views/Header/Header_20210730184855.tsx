import React, { useEffect , useState,} from 'react'
import { useWallet } from '@binance-chain/bsc-use-wallet'
import { Button, ButtonMenuItem, ButtonMenu, Heading, Text, LogoIcon ,useWalletModal} from '@pancakeswap-libs/uikit'
import { useLocation } from 'react-router-dom'



const Header : React.FC<unknown> = () => {
	const { account, connect, reset } = useWallet()
	

	useEffect(() => {
		if (!account && window.localStorage.getItem('accountStatus')) {
		  connect('injected')
		}
	}, [account, connect])

	const { onPresentConnectModal, onPresentAccountModal } = useWalletModal(
		() => null,
		() => null,
		account
	);

	const [activeIndex, setActiveIndex] = useState(0);
	const location = useLocation();
	const loc = location.pathname
		
		useEffect(() => {
			switch(loc) {
				case '/farms':
					setActiveIndex(1);
					break;
				case '/pools':
					setActiveIndex(2);
					break;
				case '/lootbox':
					setActiveIndex(3);
					break;
				default:
					setActiveIndex(0);
					break;
			} 
		}, [loc]);
		
    return (
	
		  
    <div style={{
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'space-between'}}> 

			<div style={{alignSelf: 'center'}}>
				<img src='/images/elmer/foray_long1.png' alt="My logo" />
			</div>

			<ButtonMenu activeIndex={activeIndex} >
				<ButtonMenuItem as="a" href="/" >
					Home
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/farms" >
					Farms
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/pools" >
					Pool
				</ButtonMenuItem>
				<ButtonMenuItem as="a" href="/" >
					Lootboxes
				</ButtonMenuItem>

			</ButtonMenu>
			<div style={{alignSelf: 'center'}}>
				
				{
				account? 
				<Button onClick={onPresentAccountModal}>OpenAccount</Button> :
				<Button onClick={onPresentConnectModal}>Connect</Button>

				}
			</div>
			 

		</div>

      
    )
  }
  // add the address on openaccount 
  export default Header