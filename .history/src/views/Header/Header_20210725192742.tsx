import React from 'react'
import styled from 'styled-components'
import { Button, ButtonMenuItem, Heading, Text, LogoIcon } from '@pancakeswap-libs/uikit'
import Page from 'components/layout/Page'
import useI18n from 'hooks/useI18n'


const StyledNotFound = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  height: calc(100vh - 64px);
  justify-content: center;
`

const NotFound = () => {
    const TranslateString = useI18n()
  
    return (
        <Row>
        <ButtonMenu activeIndex={0}>
          <ButtonMenuItem as="a" href="https://pancakeswap.finance">
            Link 1
          </ButtonMenuItem>
          <ButtonMenuItem as="a" href="https://pancakeswap.finance">
            Link 2
          </ButtonMenuItem>
          <ButtonMenuItem as="a" href="https://pancakeswap.finance">
            Link 3
          </ButtonMenuItem>
        </ButtonMenu>
      </Row>

      
    )
  }
  
  export default NotFound