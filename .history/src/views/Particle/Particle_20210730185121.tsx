import React, { useEffect, Suspense, lazy } from 'react'
import Particles from 'react-particles-js';
import styled from 'styled-components'


const particleStyle = {
	position: "fixed",
  zIndex:"-1",
};
const backgroundImage = {
	position: "fixed",
  zIndex:"-2",
};
const Particle: React.FC = () => {
 
	return (
		<div>

		<img style={{position:"fixed", zIndex:-2, width: "100vw", height: "100vh", }} src="./images/402868.jpg"  alt="Logo"/>
		<Particles style={particleStyle}
            params={{
                particles: {
									color: { value: "#ffffff" },
									line_linked: {
										color: "#ffffff",
										distance: 150,
										enable: false,
										opacity: 0.4,
										width: 1
									},
									move: {
										attract: { enable: false, rotateX: 600, rotateY: 600 },
										bounce: false,
										direction: "none",
										enable: true,
										out_mode: "out",
										random: true,
										speed: 0.3,
										straight: false
									},
									number: { density: { enable: true, value_area: 800 }, value: 500 },
									opacity: {
										anim: { enable: true, opacity_min: 0.3, speed: 5, sync: false },
										random: {
											enable: true,
											minimumValue: 0.3
										},
										value: 0.6
									},
									shape: {
										type: "circle"
									},
									size: {
										anim: { enable: false, size_min: 0.3, speed: 4, sync: false },
										random: true,
										value: 2
									}
								},
								
            }}    
        />
			</div>
		
	);
  
}

export default Particle


/*

<Particles style={particleStyle}
            params={{
                particles: {
                    number: {
                        value: 400,
                        density: {
                            enable: true,
                            value_area: 1000
                        }
                    },
                    color: {
                        value: '#fff'
                    },
                    opacity: {
                        value: 0.5,
                        anim: {
                            enable: true
                        }
                    },
                    size: {
                        value: 7,
                        random: true,
                        anim: {
                            enable: true,
                            speed: 3
                        }
                    },
                    line_linked: {
                        enable: false
                    },
                    move: {
                        speed: 0.2
                    }
                 }    
            }}    
        />
*/		

/*

particlesJS('particles-js',
  
  {
    "particles": {
      "number": {
        "value": 200,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": ["circle","star"],
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 1,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 10,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "bottom-left",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 150
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true,
    "config_demo": {
      "hide_card": false,
      "background_color": "#b61924",
      "background_image": "",
      "background_position": "50% 50%",
      "background_repeat": "no-repeat",
      "background_size": "cover"
    }
  }

);

*/