import React, { useEffect, Suspense, lazy } from 'react'
import Particles from 'react-particles-js';
import styled from 'styled-components'

const particleStyle = styled.div`
display: block; 
position: absolute; 
top: 0; 
bottom: 0; 
z-index: -1; //makes it act sort of like a background
`

const Particle: React.FC = () => {
 
	return (
		<Particles className="particleStyle"
            params={{
                particles: {
                    number: {
                        value: 400,
                        density: {
                            enable: true,
                            value_area: 1000
                        }
                    },
                    color: {
                        value: '#fff'
                    },
                    opacity: {
                        value: 0.5,
                        anim: {
                            enable: true
                        }
                    },
                    size: {
                        value: 7,
                        random: true,
                        anim: {
                            enable: true,
                            speed: 3
                        }
                    },
                    line_linked: {
                        enable: false
                    },
                    move: {
                        speed: 0.2
                    }
                 }    
            }}    
        />
		
	);
  
}

export default Particle