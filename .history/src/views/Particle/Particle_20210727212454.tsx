import React, { useEffect, Suspense, lazy } from 'react'
import Particles from 'react-particles-js';
import styled from 'styled-components'

const particleStyle = styled.div`
	position: absolute;
`

const Particle: React.FC = () => {
 
	return (
		<Particles id="particleStyle"
            params={{
                particles: {
                    number: {
                        value: 400,
                        density: {
                            enable: true,
                            value_area: 1000
                        }
                    },
                    color: {
                        value: '#fff'
                    },
                    opacity: {
                        value: 0.5,
                        anim: {
                            enable: true
                        }
                    },
                    size: {
                        value: 7,
                        random: true,
                        anim: {
                            enable: true,
                            speed: 3
                        }
                    },
                    line_linked: {
                        enable: false
                    },
                    move: {
                        speed: 0.2
                    }
                 }    
            }}    
        />
		
	);
  
}

export default Particle