import React, { useEffect, Suspense, lazy } from 'react'
import Particles from 'react-particles-js';
import styled from 'styled-components'


const particleStyle = {
	position: "absolute",
  zIndex:"-1",
};
const backgroundImage = {
	position: "absolute",
  zIndex:"-2",
};
const Particle: React.FC = () => {
 
	return (
		<div>

		<img style={{position:"absolute", zIndex:-2}} src="https://i.pinimg.com/originals/f6/20/a1/f620a179ca1acfd15a0754701677fb58.jpg"  alt="Logo"/>
		<Particles style={particleStyle}
            params={{
                particles: {
									"number": {
										"value": 100,
										"density": {
											"enable": true,
											"value_area": 800
										}
									},
									"color": {
										"value": "#ffffff"
									},
									"shape": {
										"type": "circle",
										"stroke": {
											"width": 0,
											"color": "#000000"
										},
										"polygon": {
											"nb_sides": 5
										},
										"image": {
											"src": "img/github.svg",
											"width": 100,
											"height": 100
										}
									},
									"opacity": {
										"value": 0.5,
										"random": false,
										"anim": {
											"enable": false,
											"speed": 1,
											"opacity_min": 0.1,
											"sync": false
										}
									},
									"size": {
										"value": 10,
										"random": true,
										"anim": {
											"enable": false,
											"speed": 10,
											"size_min": 0.1,
											"sync": false
										}
									},
									"line_linked": {
										"enable": true,
										"distance": 150,
										"color": "#ffffff",
										"opacity": 0.4,
										"width": 1
									},
									"move": {
										"enable": true,
										"speed": 0.5,
										"direction": "none",
										"random": false,
										"straight": false,
										"out_mode": "out",
										"bounce": false,
										"attract": {
											"enable": false,
											"rotateX": 600,
											"rotateY": 1200
										}
									}
								},
								"interactivity": {
									"detect_on": "canvas",
									"events": {
										"onhover": {
											"enable": true,
											"mode": "grab"
										},
										"onclick": {
											"enable": true,
											"mode": "push"
										},
										"resize": true
									},
									"modes": {
										"grab": {
											"distance": 140,
											"line_linked": {
												"opacity": 1
											}
										},
										"bubble": {
											"distance": 400,
											"size": 40,
											"duration": 2,
											"opacity": 8,
											
										},
										"repulse": {
											"distance": 200,
											"duration": 0.4
										},
										"push": {
											"particles_nb": 4
										},
										"remove": {
											"particles_nb": 2
										}
									}
								},
								
            }}    
        />
			</div>
		
	);
  
}

export default Particle


/*

<Particles style={particleStyle}
            params={{
                particles: {
                    number: {
                        value: 400,
                        density: {
                            enable: true,
                            value_area: 1000
                        }
                    },
                    color: {
                        value: '#fff'
                    },
                    opacity: {
                        value: 0.5,
                        anim: {
                            enable: true
                        }
                    },
                    size: {
                        value: 7,
                        random: true,
                        anim: {
                            enable: true,
                            speed: 3
                        }
                    },
                    line_linked: {
                        enable: false
                    },
                    move: {
                        speed: 0.2
                    }
                 }    
            }}    
        />
*/		

/*

particlesJS('particles-js',
  
  {
    "particles": {
      "number": {
        "value": 200,
        "density": {
          "enable": true,
          "value_area": 800
        }
      },
      "color": {
        "value": "#ffffff"
      },
      "shape": {
        "type": ["circle","star"],
        "stroke": {
          "width": 0,
          "color": "#000000"
        },
        "polygon": {
          "nb_sides": 5
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 1,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 1,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 10,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 40,
          "size_min": 0.1,
          "sync": false
        }
      },
      "line_linked": {
        "enable": false,
        "distance": 150,
        "color": "#ffffff",
        "opacity": 0.4,
        "width": 1
      },
      "move": {
        "enable": true,
        "speed": 6,
        "direction": "bottom-left",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "attract": {
          "enable": false,
          "rotateX": 600,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "onclick": {
          "enable": true,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 150
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true,
    "config_demo": {
      "hide_card": false,
      "background_color": "#b61924",
      "background_image": "",
      "background_position": "50% 50%",
      "background_repeat": "no-repeat",
      "background_size": "cover"
    }
  }

);

*/